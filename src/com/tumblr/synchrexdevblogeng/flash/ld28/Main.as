package com.tumblr.synchrexdevblogeng.flash.ld28
{
	import com.tumblr.synchrexdevblogeng.flash.ld28.screens.TitleScreen;
	import flash.system.fscommand;
	import org.flixel.*;
	
	
	[SWF(width = "640", height = "480", backgroundColor = "#000000")]
	
	/**
	 * ...
	 * @author Yaru
	 */
	
	// Hello, Ludum Dare people! 
	// I really hope that you have enjoyed my game.
	// I know it's much improved, but I think it's the best I could do under the circumstances of time limit and theme.
	// Actually I find it very fun, but maybe that's just because it's my game...
	// Thank you for playing!
	// ~Yaru~
	
	public class Main extends FlxGame
	{
		public function Main():void 
		{
			super(320, 240, TitleScreen, 2, 60, 30, true);
			//forceDebugger = true;
		}
		
		override protected function update():void 
		{
			if (FlxG.keys.ESCAPE) {
				fscommand("quit");
			}
			
			super.update();
		}

	}

}