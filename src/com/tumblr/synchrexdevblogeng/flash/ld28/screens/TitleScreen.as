package com.tumblr.synchrexdevblogeng.flash.ld28.screens 
{
	import org.flixel.FlxG;
	import org.flixel.FlxState;
	import org.flixel.FlxText;
	/**
	 * ...
	 * @author Yaru
	 */
	public class TitleScreen extends FlxState
	{
		public function TitleScreen() 
		{
			var t:FlxText = new FlxText(FlxG.width / 2 - 120, FlxG.height / 2 - 50, 300);
			t.text = "The enemy fleet is approaching.\nWe need to avoid them before they destroy us.\n(Because for some reason they will not kill us unless we bump into them directly)\nBut be careful:";
			var t2:FlxText = new FlxText(FlxG.width / 2 - 80, FlxG.height / 2 + 20, 300);
			t2.text = "[YOU ONLY GET ONE SHIP]\n(And you only get one button to drive it)\n(Our budget was very limited)"
			var t3:FlxText = new FlxText(FlxG.width / 2 - 100, FlxG.height / 2 + 60, 300);
			t3.text = "PRESS [SPACE] TO START. \nPRESS [SPACE] TO MOVE YOUR SHIP.\nSURVIVE FOR ONE MINUTE";
			add(t);
			add(t2);
			add(t3);
		}
		
		override public function update():void 
		{
			if (FlxG.keys.justPressed("SPACE")) {
				FlxG.switchState(new PlayState);
			}
			super.update();
		}
		
	}

}