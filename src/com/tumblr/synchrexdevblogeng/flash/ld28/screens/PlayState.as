package com.tumblr.synchrexdevblogeng.flash.ld28.screens 
{
	import com.tumblr.synchrexdevblogeng.flash.ld28.entities.*;
	import org.flixel.*;
	/**
	 * ...
	 * @author Yaru
	 */
	public class PlayState extends FlxState
	{
		public static var ship:Ship;
		private var spawnTimer:int = 30;
		private var minuteTimer:Number = 60;
		private var enemyGroup:FlxGroup;
		private var timerText:FlxText;
			
		public static var deathText:FlxText;
		
		[Embed(source = "../../../../../../../assets/bgmusic.mp3")] private var bgMusic:Class;
		[Embed(source = "../../../../../../../assets/Candidato 4.mp3")] private var deathSound:Class;
		[Embed(source = "../../../../../../../assets/bg.png")] private var bg:Class;
			
		public function PlayState() 
		{
			var bg:FlxSprite = new FlxSprite(0, 0, bg);
			bg.alpha = 0.2;
			add(bg);
			
				
			timerText = new FlxText(125, 0, 100);
			add(timerText);
			
			ship = new Ship(50, 50);
			add(ship);
			enemyGroup = new FlxGroup();
			add(enemyGroup);
			
			deathText = new FlxText(FlxG.width / 2 - 45, FlxG.height / 2, 100);
			deathText.text = "YOU'RE DEAD. PRESS SPACE TO RESTART";
			deathText.visible = false;
			add(deathText);
				
		}
		
		override public function create():void 
		{
			FlxG.play(bgMusic, 1, true, true);
			super.create();
		}
		
		override public function update():void 
		{
			if (FlxG.mouse.justPressed()) {
				trace(FlxG.mouse.y);
			}
			
			if (spawnTimer > 0) {
				spawnTimer -= FlxG.elapsed;
			}
			if (spawnTimer < 0) {
				spawnTimer = 0;
			}
			if (spawnTimer == 0) {
				var a:int =  randomNumber(0, 5, true);
				var b:Enemy
				switch (a) 
				{
				case 0:
					b = new Enemy(FlxG.width - 20, 14);
					break;
				case 1:
					b = new Enemy(FlxG.width - 20, 50);
					break;
				case 2:
					b = new Enemy(FlxG.width - 20, 94);
					break;
				case 3:
					b = new Enemy(FlxG.width - 20, 152);
					break;
				case 4:
					b = new Enemy(FlxG.width - 20, 202);
					break;
				case 5:
					b = new Enemy(FlxG.width - 20, 229);
					break;
				}
				enemyGroup.add(b);
				if (minuteTimer > 30) {
					spawnTimer = 30;
				}
				else {
					spawnTimer = 25;
				}
			}
				
			if (minuteTimer > 0 && ship.isDead == false) {
				minuteTimer -= FlxG.elapsed;
			}
			
			if (minuteTimer == 1 || minuteTimer < 1) {
				FlxG.switchState(new VictoryScreen);
			}
			
			timerText.text = "TIME LEFT:\n       " + FlxU.floor(minuteTimer).toString();
			manageCollisions();
			super.update();
		}
		
		private function manageCollisions():void 
		{
			FlxG.overlap(ship, enemyGroup, shipInEnemy);
		}
		
		private function shipInEnemy(s:Ship,e:Enemy):void 
		{
			if (s.visible) {
				var emitter:FlxEmitter = new FlxEmitter(s.x, s.y);
				var particles:int = 10;
				for (var i:int = 0; i < particles; i++) 
				{
					var particle:FlxParticle = new FlxParticle();
					particle.makeGraphic(2, 2, 0xFF0000FF);
					particle.exists = false;
					emitter.add(particle);
				}
				emitter.gravity = 100;
				add(emitter);
				emitter.start();
				s.kaboom();
				var sound:FlxSound = FlxG.play(deathSound);
				sound.fadeOut(1);
				
			}
		}
		
		private function randomNumber(min:Number, max:Number, Absolute:Boolean = false):Number
		{
			if (!Absolute)
			{
				return Math.floor(Math.random() * (1 + max - min) + min);
			}
			else
			{
				return Math.abs(Math.floor(Math.random() * (1 + max - min) + min));
			}
		}
		
	}

}