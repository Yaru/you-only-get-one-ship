package com.tumblr.synchrexdevblogeng.flash.ld28.screens 
{
	import org.flixel.FlxG;
	import org.flixel.FlxState;
	import org.flixel.FlxText;
	/**
	 * ...
	 * @author Yaru
	 */
	public class VictoryScreen extends FlxState
	{
		
		public function VictoryScreen() 
		{
			var t:FlxText = new FlxText(0, 0, 300);
			t.text = "[LUDUM DARE 28 - MADE IN 48 HOURS - BY YARU]";
			add(t);
			var t2:FlxText = new FlxText(FlxG.width / 2 - 100, FlxG.height / 2 - 25, 300);
			t2.text = "CONGRATULATIONS!\nYOU HAVE AVOIDED THE ENEMY FLEET\nNOW THEY CAN KILL EVERYBODY EXCEPT US!\nWE HAVE BROUGHT THE UNIVERSE TO ITS END\nLET'S HAVE CAKE!!\nWHAT FLAVOR WOULD YOU LIKE?"
			add(t2);
			var t3:FlxText = new FlxText(FlxG.width / 2 - 120, FlxG.height - 20, 300);
			t3.text = "[THANKS FOR PLAYING! PRESS ENTER TO RESTART]"
			add(t3);
		}
		
		override public function update():void 
		{
			if (FlxG.keys.ENTER) {
				FlxG.resetGame();
			}
			super.update();
		}
		
	}

}