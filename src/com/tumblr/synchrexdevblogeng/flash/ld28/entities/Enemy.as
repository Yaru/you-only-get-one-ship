package com.tumblr.synchrexdevblogeng.flash.ld28.entities 
{
	import org.flixel.FlxSprite;
	/**
	 * ...
	 * @author Yaru
	 */
	public class Enemy extends FlxSprite
	{
		[Embed(source = "../../../../../../../assets/evilUFO2.png")] private var graphic:Class;
		public function Enemy(X:Number,Y:Number) 
		{
			super(X, Y);
			loadGraphic(graphic, false, false, 16, 16, false);
		}
		
		override public function update():void 
		{
			velocity.x = -120;
			
			if (!onScreen()) {
				kill();
			}
			
			super.update();
		}
		
	}

}