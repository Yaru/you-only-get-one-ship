package com.tumblr.synchrexdevblogeng.flash.ld28.entities 
{
	import com.tumblr.synchrexdevblogeng.flash.ld28.screens.PlayState;
	import org.flixel.FlxEmitter;
	import org.flixel.FlxG;
	import org.flixel.FlxParticle;
	import org.flixel.FlxSound;
	import org.flixel.FlxSprite;
	/**
	 * ...
	 * @author Yaru
	 */
	public class Ship extends FlxSprite
	{
		public var isDead:Boolean = false;
		[Embed(source = "../../../../../../../assets/Candidato 2.mp3")] private var jumpSound:Class;
		[Embed(source = "../../../../../../../assets/goodUFO.png")] private var shipGraphic:Class;
		
		public function Ship(X:Number,Y:Number) 
		{
			super(X, Y);
			loadGraphic(shipGraphic, false, false, 16, 16, false);
		}
		
		override public function update():void 
		{
			if (FlxG.keys.justPressed("SPACE")) {
				var s:FlxSound = FlxG.play(jumpSound);
				s.fadeOut(1);
				switch (this.y) 
				{
				case 14:
					this.y = 50;
					break;
				case 50:
					this.y = 94;
					break;
				case 94:
					this.y = 152;
					break;
				case 152:
					this.y = 202;
					break;
				case 202:
					this.y = 229;
					break;
				case 229:
					this.y = 14;
				}
				var emitter:FlxEmitter = new FlxEmitter(this.x, this.y);
				var particles:int = 5;
				for (var i:int = 0; i < particles; i++) 
				{
					var particle:FlxParticle = new FlxParticle();
					particle.makeGraphic(2, 2, 0xFF0000FF);
					particle.exists = false;
					emitter.add(particle);
				}
				emitter.gravity = 100;
				FlxG.state.add(emitter);
				emitter.start();
			}
			
			if (isDead && FlxG.keys.justPressed("SPACE")) {
				FlxG.resetState();
			}
			
			super.update();
		}
		
		public function kaboom():void 
		{
			visible = false;
			PlayState.deathText.visible = true;
			isDead = true;
		}
			
	}

}